package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.example.sender.Sender;

@SpringBootApplication
@ComponentScan(basePackages = "com")
public class KafkaExampleApplication implements CommandLineRunner{
	
	@Autowired
    private Sender sender;

	public static void main(String[] args) {
		SpringApplication.run(KafkaExampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		sender.send("Spring Kafka Producer and Consumer Example");
	}

}

