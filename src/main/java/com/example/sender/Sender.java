package com.example.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Sender {
	
	private static final Logger log=LoggerFactory.getLogger(Sender.class);
	
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Value("${TOPIC}")
	private String topic;
	
	public void send(String message){
		log.info("sending message='{}' to topic='{}'", message, topic);
		kafkaTemplate.send(topic,message);
	}
	

}
